import re
import pytest
import time


from ec_slp_lib import EcSLP

from test_helper import (
    params,
    rpc,
    sleep_time,
    load_wallet_sleep_time,
    bch_amount,
    test_address,
)

# create an instance of class EcSLP
ec_slp = EcSLP()

def run_wallet():
    ec_slp.start_daemon(params)
    time.sleep(sleep_time)
    ec_slp.load_wallet(params, params["wallet_path"])
    time.sleep(load_wallet_sleep_time)

def test_ec_rpc():
    run_wallet()

    method = "version"
    pattern = re.compile("([0-9])+\\.([0-9])+\\.([0-9])+")
    assert pattern.match(ec_slp.ec_rpc(rpc, method)["result"])


def test_validate_address():
    assert ec_slp.validate_address(rpc, test_address)


def test_get_unused_bch_address():
    assert ec_slp.validate_address(rpc, ec_slp.get_unused_bch_address(rpc))


# FIXME, This currently depends on the EC `complete` response but
# Decoding tools or Meep could be used to verify transaction.
def test_prepare_bch_transaction():
    send_coin = ec_slp.prepare_bch_transaction(rpc, test_address, bch_amount)
    assert send_coin["complete"] is True


def test_broadcast_tx():
    tx_hex = ec_slp.prepare_bch_transaction(rpc, test_address, bch_amount)["hex"]
    assert True in ec_slp.broadcast_tx(rpc, tx_hex)

def test_fail_ec_rpc_authentication():
    ec_slp.stop_daemon(params)
    
    with pytest.raises(Exception) as e:
        method = "version"
        rpc["user"] = "whatever"
        ec_slp.ec_rpc(rpc, method)
        print(e)


def test_fail_ec_rpc_connection():
    ec_slp.stop_daemon(params)

    with pytest.raises(Exception) as e:
        print("test_fail", ec_slp.check_daemon(params), e)
        if ec_slp.check_daemon(params) != "Daemon not running":
            ec_slp.stop_daemon(params)
        method = "version"
        ec_slp.ec_rpc(rpc, method)

