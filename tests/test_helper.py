import os

# ~ # Using .env file
from dotenv import load_dotenv

load_dotenv()


# time to wait before checking if daemon is running
sleep_time = int(os.getenv("sleep_time"))

# time to wait before loading a wallet
load_wallet_sleep_time = int(os.getenv("load_wallet_sleep_time"))
test_address = os.getenv("test_address")
bch_amount = os.getenv("bch_amount")  # amount to send in tests


# Dictionary for use with the EcSLP class
params = {
    "rpc_user": os.getenv("rpc_user"),
    "rpc_url": os.getenv("rpc_url"),
    "rpc_port": os.getenv("rpc_port"),
    "rpc_password": os.getenv("rpc_password"),
    "network": os.getenv("network"),
    "python_path": os.getenv("python_path"),
    "electron_cash_path": os.getenv("electron_cash_path"),
    "wallet_path": os.getenv("wallet_path"),
    "network_options": os.getenv("network_options"),
    "use_custom_dir": True,
    "custom_dir": os.getenv("custom_dir"),
}

rpc = {
    "user": os.getenv("rpc_user"),
    "url": os.getenv("rpc_url"),
    "port": os.getenv("rpc_port"),
    "password": os.getenv("rpc_password"),
}
